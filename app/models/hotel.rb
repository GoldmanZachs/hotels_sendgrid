class Hotel
  include Mongoid::Document
  include AASM

  field :name, type: String

  has_many :rooms
  embeds_one :address

  accepts_nested_attributes_for :address, :rooms

  field :state

  aasm do
    state :vacancies, initial: true
    state :no_vacancies
    
    event all_rooms_reserved do
      transitions from: :vacancies, to: :no_vacancies
    end  
    
    event check_out do
      transitions from: :no_vacancies, to: :vacancies
    end  
  end

  def send_email(room)
    puts "Send email: #{room.inspect}"
  end
end
